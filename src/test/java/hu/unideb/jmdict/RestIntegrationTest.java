package hu.unideb.jmdict;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;



@RunWith(SpringRunner.class)
@SpringBootTest(classes= {SpringBootRestApplication.class}) //(classes= {FooController.class, FooService.class} )
@AutoConfigureMockMvc
public class RestIntegrationTest {
	 
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenTestApp_thenEmptyResponse() throws Exception {
        this.mockMvc.perform(get("/dictionary/from_gaigo/pillow"))
            .andExpect(status().isOk());
    }
    
    /*
    @Test
    public void whenTestApp_thenEmptyResponse2() throws Exception {
        this.mockMvc.perform(get("/dictionary/from_nihongo/すみません?language=eng"))
            .andExpect(status().isOk());
    }
    */
}

package hu.unideb.jmdict;

import com.neovisionaries.i18n.LanguageAlpha3Code;

public class SearchTextDTO {

	private String text;
	
	private LanguageAlpha3Code langCode;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public LanguageAlpha3Code getLangCode() {
		return langCode;
	}

	public void setLangCode(LanguageAlpha3Code langCode) {
		this.langCode = langCode;
	}
	
	
}

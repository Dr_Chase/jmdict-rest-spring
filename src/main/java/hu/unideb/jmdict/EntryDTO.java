package hu.unideb.jmdict;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.neovisionaries.i18n.LanguageAlpha3Code;

import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.KanjiElement;
import hu.unideb.jmdict.jmdict.ReadingElement;
import hu.unideb.jmdict.jmdict.Sense;
import hu.unideb.jmdict.repositories.GlossRepository;

@Component
public class EntryDTO {

	@Autowired
	@JsonIgnore
	private GlossRepository glossRepository;

	private List<String> readingElements = new ArrayList<>();

	private List<String> kanjiElements = new ArrayList<>();

	private List<String> gaiGoMeanings = new ArrayList<>();

	private LanguageAlpha3Code langCode;

	public static EntryDTO of(Gloss gloss, Set<Gloss> usedGlosses) {
		
		if(usedGlosses.contains(gloss))	{
			return null;
		}

		EntryDTO entryDto = new EntryDTO();

		LanguageAlpha3Code langCode = gloss.getLanguageCode();
		entryDto.setLangCode(langCode);

		Sense sense = gloss.getSense();
		Hibernate.initialize(sense);

		List<Gloss> glosses = sense.getGlosses();
		glosses.size();

		usedGlosses.addAll(glosses);

		for (Gloss g : glosses) {

			entryDto.gaiGoMeanings.add(g.getMeaningText());
		}

		Entry entry = sense.getEntry();

		List<ReadingElement> readingElements = entry.getReadingElements();
		readingElements.size();

		for (ReadingElement re : readingElements) {

			entryDto.readingElements.add(re.getReadingElementText());
		}

		List<KanjiElement> kanjiElements = entry.getKanjiElements();

		if (kanjiElements.size() > 0) {

			for (KanjiElement ke : kanjiElements) {

				entryDto.getKanjiElements().add(ke.getKanjiElement());
			}
		}

		return entryDto;
	}

	public static EntryDTO of(ReadingElement readingElement, Set<ReadingElement> usedReadingElements,
			LanguageAlpha3Code langCode) {
		
		if(usedReadingElements.contains(readingElement))	{
			return null;
		}

		EntryDTO entryDto = new EntryDTO();

		entryDto.setLangCode(langCode);

		Entry entry = readingElement.getEntry();
		Hibernate.initialize(entry);

		List<ReadingElement> readingElementsInThisEntry = entry.getReadingElements();
		readingElementsInThisEntry.size();

		usedReadingElements.addAll(readingElementsInThisEntry);

		List<Sense> senses = entry.getSenses();
		senses.size();

		List<Gloss> allGlosses = new ArrayList<>();

		for (Sense s : senses) {

			allGlosses.addAll(s.getGlosses());
		}

		List<String> filteredGlosses = allGlosses.stream().filter((gloss) -> gloss.getLanguageCode() == langCode)
				.map((gloss) -> gloss.getMeaningText()).collect(Collectors.toList());

		entryDto.getGaiGoMeanings().addAll(filteredGlosses);

		List<ReadingElement> readingElementsOfEntry = entry.getReadingElements();
		readingElementsOfEntry.size();

		List<String> readingElementsAsStrings = readingElementsOfEntry.stream()
				.map((readingElementInStream) -> readingElementInStream.getReadingElementText())
				.collect(Collectors.toList());

		entryDto.getReadingElements().addAll(readingElementsAsStrings);

		List<KanjiElement> kanjiElements = entry.getKanjiElements();

		if (kanjiElements.size() > 0) {

			for (KanjiElement ke : kanjiElements) {

				entryDto.getKanjiElements().add(ke.getKanjiElement());
			}
		}

		return entryDto;
	}

	public GlossRepository getGlossRepository() {
		return glossRepository;
	}

	public void setGlossRepository(GlossRepository glossRepository) {
		this.glossRepository = glossRepository;
	}

	public List<String> getReadingElements() {
		return readingElements;
	}

	public void setReadingElements(List<String> readingElements) {
		this.readingElements = readingElements;
	}

	public List<String> getKanjiElements() {
		return kanjiElements;
	}

	public void setKanjiElements(List<String> kanjiElements) {
		this.kanjiElements = kanjiElements;
	}

	public List<String> getGaiGoMeanings() {
		return gaiGoMeanings;
	}

	public void setGaiGoMeanings(List<String> gaiGoMeanings) {
		this.gaiGoMeanings = gaiGoMeanings;
	}

	public LanguageAlpha3Code getLangCode() {
		return langCode;
	}

	public void setLangCode(LanguageAlpha3Code langCode) {
		this.langCode = langCode;
	}

}

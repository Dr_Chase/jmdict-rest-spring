package hu.unideb.jmdict;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.event.ListSelectionEvent;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import hu.unideb.jmdict.jmdict.Entry;
import hu.unideb.jmdict.jmdict.Gloss;
import hu.unideb.jmdict.jmdict.ReadingElement;
import hu.unideb.jmdict.jmdict.Sense;
import hu.unideb.jmdict.repositories.EntryRepository;
import hu.unideb.jmdict.repositories.GlossRepository;
import hu.unideb.jmdict.repositories.ReadingElementRepository;
import org.springframework.web.bind.annotation.RestController;

import com.chase.hiragana.RomajiLettersHir;
import com.chase.katakana.RomajiLettersKat;
import com.neovisionaries.i18n.LanguageAlpha3Code;



@RestController
@RequestMapping("/dictionary")
public class JMDictRestController {

	@Autowired
	private EntryRepository entryRepository;
	
	@Autowired
	private GlossRepository glossRepository;
	
	@Autowired
	private ReadingElementRepository readingElementRepository;
	
	//@GetMapping
	//@ResponseBody
	//public List<Foo> findAll() {

	//}
	
	/*
	 * works with URL like:
	 * http://localhost:8080/dictionary/from_gaigo/sleep
	 */

	@CrossOrigin
	@GetMapping(value = "from_gaigo/{word}")
	@ResponseBody
	@Transactional(readOnly = true)
	public List<EntryDTO> findGaigoByMeaning(@PathVariable("word") String word) {

		// https://www.baeldung.com/hibernate-second-level-cache

		List<Gloss> listOfGlosses = glossRepository.findGlossByMeaningTextContaining(word);
		Set<Gloss> usedGlosses = new HashSet<>();
		
		List<EntryDTO> outputDTOs = new ArrayList<>();
		for(Gloss gloss : listOfGlosses)	{
			
			EntryDTO entryDTO = EntryDTO.of(gloss, usedGlosses);
			
			if(entryDTO != null)	{
				
				outputDTOs.add(entryDTO);
			}
		}
		
		
		return outputDTOs ;
	}
	
	@CrossOrigin
	@PostMapping(value = "from_nihongo/")
	@ResponseBody
	@Transactional(readOnly = true)
	public List<EntryDTO> findNihongoByMeaning(@RequestBody SearchTextDTO resource) {
		
		LanguageAlpha3Code langCode = resource.getLangCode();
		
		String word = resource.getText();
		
		Set<ReadingElement> usedReadingElements = new HashSet<>();
		
		RomajiLettersHir romajiToHiraganaConverter = RomajiLettersHir.getInstance();
		
		RomajiLettersKat romajiToKatakanaConverter = RomajiLettersKat.getInstance();
		
		String hiraganaWord = romajiToHiraganaConverter.convertToHiragana(word);
		String katakanaWord = romajiToKatakanaConverter.parseKatakanaString(word);
		
		List<ReadingElement> readingElements = 
				readingElementRepository.findReadingElementByReadingElementTextContaining(hiraganaWord);
		
		readingElements.addAll( readingElementRepository.findReadingElementByReadingElementTextContaining(katakanaWord) );

		List<EntryDTO> output = new ArrayList<>();
		for(ReadingElement re : readingElements)	{
			
			EntryDTO dto = EntryDTO.of(re, usedReadingElements, langCode);
			if(dto != null)	{
				
				output.add(dto);
			}
		}
	
		
		return output ;
		
		/*
		 * works with JSON like:
		 * {
				"text": "すみません",
				"langCode": "eng"
			}
		 * 
		 */
	}

	/*
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(@RequestBody Foo resource) {

	} */

	/*
	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable("id") Long id, @RequestBody Foo resource) {
		
	} */

	/*
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable("id") Long id) {

	}
	*/

}


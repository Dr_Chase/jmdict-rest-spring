package hu.unideb.jmdict;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan( {"hu.unideb.jmdict.repositories", "hu.unideb.jmdict" })
@EnableJpaRepositories("hu.unideb.jmdict.repositories")
@EntityScan("hu.unideb.jmdict.jmdict")
@SpringBootApplication
public class SpringBootRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApplication.class, args);
	}
	
}